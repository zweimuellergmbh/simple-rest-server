package hello;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

@RestController
public class Controller {

        private static final String template = "Hello, %s!";
        private final AtomicLong counter = new AtomicLong();

        @RequestMapping("/greeting")
        public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
            return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
        }

	@RequestMapping(value = "/version", method = RequestMethod.GET)
	public Version greeting() {
		return new Version(1, "blabla");
	}

	@RequestMapping(value = "/signature", method = RequestMethod.POST)
	public ResponseEntity<?> signature(@RequestBody Signature signature) {

		System.out.println("POST Signature: Mode: " + signature.getMode() + ", Value: " + signature.getValue()); 
	
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}


}
