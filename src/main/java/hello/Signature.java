package hello;

public class Signature {

    private long mode;
    private String value;

    public Signature() {}

    public Signature(long mode, String value) {
        this.mode = mode;
        this.value = value;
    }

    public long getMode() {
        return mode;
    }

    public String getValue() {
        return value;
    }

    public void setMode(final long mode) {
	this.mode = mode;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
