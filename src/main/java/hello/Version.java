package hello;

public class Version {

    private final long version;
    private final String description;

    public Version(long version, String description) {
        this.version = version;
        this.description = description;
    }

    public long getVersion() {
        return version;
    }

    public String getDescription() {
        return description;
    }
}
